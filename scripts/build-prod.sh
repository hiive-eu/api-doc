#!/usr/bin/env bash
if [ $# -eq 0 ]
then
  echo "Must supply hiive-api version as argument"
  exit 1
fi

HIIVE_API_DOC_VERSION=$1

docker build -t beehiive/hiive-apidoc:${HIIVE_API_DOC_VERSION} --push --file Dockerfile .
