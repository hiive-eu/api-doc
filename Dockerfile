FROM joseluisq/static-web-server:2-alpine

COPY ./swagger-ui-dist /public
COPY ./openapi.yaml /public/openapi.yaml
